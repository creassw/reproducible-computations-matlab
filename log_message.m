function [  ] = log_message( message, varargin )
%LOG_MESSAGE Print a log message to the screen and log file.
%   This function is called as log_message(message) and prints the string "message"
%   to both the screen and the logfile.
%   String formatting can be used, for example: log_message('String %s, %s', 'string1', 'string2')

global logfile_id
global output_dir

if ~ischar(varargin)
    varargin = char(varargin);
end

sprintf(message, varargin)
fprintf(logfile_id, strcat(message,'\n'), varargin);

end

