function [  ] = start_logging( script_name )
%START_LOGGING Create a log file and put header into it
%   The log file is created in a subdirectory based on the date of the current git commit,
%   and named according to the argument 'script_name'.

global logfile_id
global output_dir

%% Getting git identifier -> time stamp of current commit
[git_status,git_time_stamp] = system('git --no-pager show -s --format=%cI HEAD');

if git_status ~= 0
    warning('Calling git failed with error: "%s"', git_time_stamp)
    git_error = git_time_stamp;
    git_id = '_no-git-id_';
else
    git_id = strrep(git_time_stamp,':','-');
    
end

% check whether the working copy is clean, produce a warning otherwise and append to git_hash_string
% this is only done if a git_hash_string was obtained successfully above
if git_status == 0
    [s,git_hash_string] = system('git rev-parse HEAD');
    [unstaged, message_unstaged] = system('git diff --exit-code');
    [staged, message_staged] = system('git diff --cached --exit-code');
    % This does not yet take into account files that may not have been added!
    % If you want this to be "stricter", check git status --porcelain and use a .gitignore file as needed.

    if staged ~= 0 | unstaged ~= 0 % or 
        warning('Git repository does not appear to be clean at the moment, see "git diff" or "git diff --cached" for details.');
        git_id = strcat('WIP_on_', git_id);
    end
end

%% Creating the log file with script name and time stamp

output_dir = strcat('results_', git_id);
if 7 ~= exist(output_dir)
    mkdir(output_dir);
end
[nil, name, nil] = fileparts(script_name); 
logfile_name = fullfile(output_dir, strcat(name, '.log'));

logfile_id = fopen(logfile_name, 'wt');
if logfile_id == -1
    error('Could not open logfile %s for writing', logfile_name)
end
fprintf(logfile_id, 'Script name: %s\n', script_name);
fprintf(logfile_id, 'Start of computation: %s\n', char(datetime()));
fprintf(logfile_id, '\n--------------------------------\n\n');

%% Writing git information to log file
if git_status == 0
    fprintf(logfile_id, 'Current git head commit is: %s', git_hash_string);
    fprintf(logfile_id, 'Time stamp of git head commit is: %s\n', git_time_stamp);
    if staged ~= 0 | unstaged ~= 0
        fprintf(logfile_id, 'WARNING - There are uncommited changes in this repository:\n');
        if unstaged ~= 0
            fprintf(logfile_id, 'The output of "git diff" is:\n%s\n', message_unstaged);
        end
        if staged ~= 0
            fprintf(logfile_id, 'The output of "git diff --cached" is:\n%s\n', message_staged);
        end        
    end
else
    fprintf(logfile_id, 'WARNING - Error calling git: %s\n', git_error);
end
fprintf(logfile_id, '\n--------------------------------\n\n');

%% Writing version information to log file
matlab_version = version;
computer_os = computer;
fprintf(logfile_id, 'Matlab version: %s\n', matlab_version);
fprintf(logfile_id, 'Computer OS: %s\n', computer_os);
fprintf(logfile_id, '\n--------------------------------\n\n');



end

