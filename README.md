# Scripts to conduct a reproducible computational research project in Matlab

This is a collection of scripts that help setting up a computational
research project in Matlab.

## Installation

The package consists of several Matlab .m files. These files need to
be put somewhere where Matlab can find them, for example the user's
MATLAB directory:

https://mathworks.com/help/matlab/ref/userpath.html

They can also just be kept locally in the same directory with the
files of the research project.

The ``main.m`` file is meant as a template and does not to be kept.

## Usage

It is recommend to set up the project as a collection of scripts based
on the template given in the ``main.m`` file. The different functions
that are provided in this package should be used according to the
example in the template whenever a log message is generated, a figure
file should be saved, or a variable should be saved to a file.

To make use of the full functionality, the script files should be part
of a git repository.

Running a Matlab script that is written according to the template will
do the following:

1. Create a specific results directory as a subdirectory of the
   current working directory, named with the time stamp of the most
   recent git commit ("HEAD").
2. Generate a log file that contains basic logging information, such
   as a time stamp and Matlab version information.
3. All figures that are saved with the ``log_fig.m`` function and data
   that are saved with the ``log_save.m`` function are saved in
   the results directory.

## Contributors

- Steffen Waldherr
- Armin Küper
- Kobe De Becker

KU Leuven, Department of Chemical Engineering
