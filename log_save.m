function [  ] = log_save( filename, varargin )
%LOG_SAVE Save all variables to file with logging.
%   This function is called as log_save(filename, var1, var2, ...) and saves var1, var2, ...
%   to the file "filename", using their original name in the caller's workspace.
%   Note that the file is overwritten, so any previous data in the file will be lost.

global logfile_id
global output_dir

for i=1:length(varargin)
    vars.(inputname(i+1)) = varargin{i};
    fprintf(logfile_id, 'Saving variable "%s" to file "%s".\n', inputname(i+1), filename);
end

save(fullfile(output_dir, filename), '-struct', 'vars');

end

