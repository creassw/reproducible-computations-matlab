function [  ] = log_fig( filename, fig_handle )
%LOG_FIG Save a figure with logging
%   This function is called as log_fig(filename, fig_handle) and saves the figure indicated by
%   the handle "fig_handle" to the file "filename".

global logfile_id	
global output_dir

saveas(fig_handle, fullfile(output_dir, filename))
% if no file extension is specified in 'filename' the figure is saved as a .fig

end

