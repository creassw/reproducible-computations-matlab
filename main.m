%% Create and / or open the log file
script_name = mfilename('fullpath');
start_logging(script_name);

%% Enter your main computation here

%% Example for creating and saving a figure: log_fig
t = linspace(0, 10, 100);
y = sin(t);
f = plot(t, y);
log_fig('sinus_plot.png', f);

%% Example for saving data: log_save
data = rand(5,5);
log_save('random_data.mat', data)
clear data;
global output_dir
load(fullfile(output_dir, 'random_data.mat'))
data

%% Example for printing a message to the log file: log_message
log_message('This is just a test message from script %s.', mfilename());

%% Close logfile at the end of computation

stop_logging();