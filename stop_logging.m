function [  ] = stop_logging(  )
%STOP_LOGGING Finish logging file and close it.
%   The function writes a final time stamp into the file before closing it.

global logfile_id

fprintf(logfile_id, '\n--------------------------------\n\n');
fprintf(logfile_id, 'End of computation: %s\n', char(datetime()));
fclose(logfile_id);

end

